var lineReader = require('line-by-line'),
    gulp = require('gulp'),
    version,
    zip = require('gulp-zip');

gulp.task('deploy', function () {
    getScriptSources()
        .pipe(gulp.dest('D:\\Program Files (x86)\\AnkhBotR2\\Twitch\\Scripts\\EnhancedRaffleSystem'));
});

gulp.task('bundle', function () {
    getCurrentVersion().then(
        function (version) {
            getScriptSources()
                .pipe(zip('EhnhancedRaffleSystem-v' + version + '.zip'))
                .pipe(gulp.dest('bundle'));
        },
        function (err) {
            throw err;
        }
    );
});

function getScriptSources() {
    return gulp.src([
        '*.py',
        'settings.json',
        'README.md',
        'UI_Config.json',
        'settings.json'
    ]);
}

function getCurrentVersion() {
    return new Promise(function (resolve, reject) {
        var lr = new lineReader('EnhancedRaffleSystem_AnkhBotSystem.py'),
            that = this;
        this.isResolved = false;

        lr.on('line', function (line) {
            var matches = line.match(/^Version = "([^"]+)"$/);
            if (matches && matches.length === 2) {
                that.isResolved = true;
                lr.close();
                resolve(matches[1]);
            }
        });

        lr.on('error', function (err) {
            reject(err);
        });

        lr.on('end', function () {
            if (that.isResolved === false) {
                reject('Failed to find version line in python script');
            }
        })
    });
}
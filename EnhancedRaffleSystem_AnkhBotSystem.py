# ---------------------------------------
# Import Libraries
# ---------------------------------------
import clr, json, os, time, codecs, sqlite3
from random import *
from sqlite3 import Error
clr.AddReference("IronPython.SQLite.dll")
clr.AddReference("IronPython.Modules.dll")

# ---------------------------------------
# [Required] Script Information
# ---------------------------------------
ScriptName = "Enhanced Raffle System"
Website = "https://gitlab.com/bapxiv/ankhbot-enhanced-raffle-system/wikis/AnkhBot-Enhanced-Raffle-System"
Description = "!raffle will accept a number of entries and a user to enter for"
Creator = "BleepBlamBleep"
Version = "1.0.1-dev"

# ---------------------------------------
# Set Variables
# ---------------------------------------
settingsFile = os.path.join(os.path.dirname(__file__), "settings.json")
snapshotDir = os.path.join(os.path.dirname(__file__), "snapshots")
activeRaffle = False
warning_30 = False
warning_10 = False
raffle_start = 0
raffleEntries = []
donatedEntries = {}
ticketPurchases = {}
winnerList = []


class Settings:
    def __init__(self, settingsFile = None):
        if settingsFile is not None and os.path.isfile(settingsFile):
            with codecs.open(settingsFile, encoding='utf-8-sig', mode='r') as f:
                self.__dict__ = json.load(f, encoding='utf-8-sig')

        else:
            self.Prize = ""
            self.Command = "!raffle"
            self.Permission = "Everyone"
            self.PermissionInfo = ""
            self.EnableWarningMessages = False

            self.TicketCost = 10
            self.MaxTickets = 100

            self.EnableEntryTimer = False
            self.EntryTimerDuration = 300
            self.EnableDonations = False
            self.AllowMultipleDonations = True
            self.CurrencyGiveaway = False
            self.CurrencyGiveawayAmount = 1000

            self.Response_RaffleOpen = "A raffle for: {0} has started! {1} can join!"
            self.Response_RaffleOpenUnlimited = "[Entry Cost: {0}] - Use `{1} <number>` to enter"
            self.Response_RaffleOpenWithMaxEntries = "Entry Cost: {0} - Max Entries: {1} - " + \
                                                     "Use `{2} <number>` to enter"
            self.Response_NotEnoughCurrency = "{0} you don't have enough {1}!"
            self.Response_ExceededMaxTickets = "@{0} You are only able to purchase up to {1} tickets."
            self.Response_DonateTickets = "or `{0} <number> <username>` to donate entries to someone"
            self.Response_MultipleDonationsDisabled = "@{0}, you can only donate entries to a single person"
            self.Response_TimerWarning = "There are {0} seconds left to enter the raffle"
            self.Response_RaffleClose = "Entries have stopped for the raffle!  You can no longer enter!"
            self.Response_RaffleRefund = "All {0} has been refunded for the current raffle."
            self.Response_Winner = "@{0}, you have won! Speak up in chat!"

            self.ManagementCommand = "!manageraffle"
            self.ManagementPermission = "Editor"
            self.ManagementPermissionInfo = ""
            self.RaffleConfigKeyword = "config"
            self.RafflePrizeKeyword = "prize"
            self.RaffleOpenKeyword = "open"
            self.RaffleCloseKeyword = "close"
            self.RafflePickWinnerKeyword = "pick"
            self.RaffleResetKeyword = "reset"
            self.RaffleRefundKeyword = "refund"
            self.RaffleSnapshotKeyword = "snapshot"
            self.RaffleLoadSnapshotKeyword = "load"

    def ReloadSettings(self, data):
        self.__dict__ = json.loads(data, encoding='utf-8-sig')
        return

    def SaveSettings(self, settingsFile):
        with codecs.open(settingsFile, encoding='utf-8-sig', mode='w+') as f:
            json.dump(self.__dict__, f, encoding='utf-8-sig', indent=4, sort_keys=True)
        with codecs.open(settingsFile.replace("json", "js"), encoding='utf-8-sig', mode='w+') as f:
            f.write("var settings = {0};".format(json.dumps(self.__dict__,
                                                            encoding='utf-8-sig', indent=4, sort_keys=True)))
        return

    def SaveSnapshot(self, key):
        if not os.path.exists(snapshotDir):
            os.makedirs(snapshotDir)
        file_path = os.path.join(snapshotDir, key + ".json")
        with codecs.open(file_path, encoding='utf-8-sig', mode='w+') as f:
            json.dump(self.__dict__, f, encoding='utf-8-sig', indent=4, sort_keys=True)
        Parent.SendTwitchMessage("Raffle Saved")
        return

    def LoadSnapshot(self, key):
        file_path = os.path.join(snapshotDir, key + ".json")
        Parent.Log("Enhanced Raffle System", file_path)
        if os.path.isfile(file_path):
            with codecs.open(file_path, encoding='utf-8-sig', mode='r') as f:
                self.__dict__ = json.load(f, encoding='utf-8-sig')

            self.SaveSettings(settingsFile)
            Parent.SendTwitchMessage("Raffle loaded")
        else:
            Parent.SendTwitchMessage("Unable to load snapshot: '{0}'".format(key))
        return


# ---------------------------------------
# [Required] Initialize Data (Only called on Load)
# ---------------------------------------
def Init():
    global MySettings, activeRaffle, settingsFile
    activeRaffle = False
    MySettings = Settings(settingsFile)
    return


def ReloadSettings(jsonData):
    global MySettings
    MySettings.ReloadSettings(jsonData)
    return


def Execute(data):
    global activeRaffle, raffleEntries, settingsFile
    if data.IsChatMessage():
        # Raffle Entries
        if activeRaffle and data.GetParam(0).lower() == MySettings.Command and \
                Parent.HasPermission(data.User, MySettings.Permission, MySettings.PermissionInfo):

                if ValidateTicketPurchase(data.User, int(data.GetParam(1))):
                    if MySettings.EnableDonations and data.GetParamCount() == 3:
                        donate_to = False
                        if data.GetParamCount() == 3:
                            donate_to = data.GetParam(2).lower()

                        if donate_to[0] == "@":
                            donate_to = donate_to[1:]

                        if donate_to:
                            if donate_to == data.User:
                                Parent.SendTwitchMessage("You cannot donate tickets to yourself")
                            else:
                                allowed_to_donate = True
                                if not MySettings.AllowMultipleDonations:
                                    if data.User in donatedEntries and len(donatedEntries[data.User]) and \
                                       donate_to not in donatedEntries[data.User]:
                                        allowed_to_donate = False

                                if allowed_to_donate:
                                    if data.User not in donatedEntries:
                                        donatedEntries[data.User] = {}
                                    if donate_to not in donatedEntries[data.User]:
                                        donatedEntries[data.User][donate_to] = 0

                                    if not MySettings.MaxTickets or \
                                       donatedEntries[data.User][donate_to] + int(data.GetParam(1)) <= \
                                       MySettings.MaxTickets:
                                        PurchaseTickets(data.User, int(data.GetParam(1)))
                                        donatedEntries[data.User][donate_to] += int(data.GetParam(1))
                                        for i in range(int(data.GetParam(1))):
                                            raffleEntries.append(donate_to)
                                    else:
                                        Parent.SendTwitchMessage(
                                            MySettings.Response_ExceededMaxTickets.format(data.User,
                                                                                          MySettings.MaxTickets))
                                else:
                                    Parent.SendTwitchMessage(
                                        MySettings.Response_MultipleDonationsDisabled.format(data.User))
                    else:
                        if not MySettings.MaxTickets or \
                               raffleEntries.count(data.User) + int(data.GetParam(1)) <= MySettings.MaxTickets:
                            PurchaseTickets(data.User, int(data.GetParam(1)))
                            for i in range(int(data.GetParam(1))):
                                raffleEntries.append(data.User)
                        else:
                            Parent.SendTwitchMessage(
                                MySettings.Response_ExceededMaxTickets.format(data.User, MySettings.MaxTickets))
        elif data.GetParam(0).lower() == MySettings.ManagementCommand.lower() and Parent.HasPermission(
                data.User, MySettings.ManagementPermission, MySettings.ManagementPermissionInfo):
            if data.GetParam(1).lower() == MySettings.RaffleCloseKeyword:
                CloseRaffle()
            elif data.GetParam(1).lower() == MySettings.RaffleResetKeyword:
                ResetRaffle()
            elif data.GetParam(1).lower() == MySettings.RafflePrizeKeyword:
                start_index = len(MySettings.ManagementCommand) + len(MySettings.RafflePrizeKeyword) + 2
                SetPrize(data.Message[start_index:])
            elif data.GetParam(1).lower() == MySettings.RaffleConfigKeyword.lower():
                start_index = 11 + len(data.GetParam(0)) + len(data.GetParam(1)) + len(data.GetParam(2)) + \
                              len(data.GetParam(3)) + len(data.GetParam(4)) + len(data.GetParam(5)) + \
                              len(data.GetParam(6)) + len(data.GetParam(7)) + len(data.GetParam(8)) + \
                              len(data.GetParam(9)) + len(data.GetParam(10))
                ConfigureRaffle(data.GetParam(2), data.GetParam(3), data.GetParam(4), data.GetParam(5),
                                data.GetParam(6), data.GetParam(7), data.GetParam(8), data.GetParam(9),
                                data.GetParam(10), data.Message[start_index:])
            elif data.GetParam(1).lower() == MySettings.RaffleOpenKeyword:
                OpenRaffle()
            elif data.GetParam(1).lower() == MySettings.RaffleRefundKeyword:
                RefundRaffleEntries()
            elif data.GetParam(1).lower() == MySettings.RafflePickWinnerKeyword.lower():
                winnerCount = 1
                if data.GetParamCount() == 3:
                    winnerCount = int(data.GetParam(2))
                PickWinner(winnerCount)
            elif data.GetParam(1).lower() == MySettings.RaffleLoadSnapshotKeyword.lower():
                start_index = 2 + len(data.GetParam(0)) + len(data.GetParam(1))
                MySettings.LoadSnapshot(data.Message[start_index:].replace(" ", "_"))
            elif data.GetParam(1).lower() == MySettings.RaffleSnapshotKeyword.lower():
                start_index = 2 + len(data.GetParam(0)) + len(data.GetParam(1))
                MySettings.SaveSnapshot(data.Message[start_index:].replace(" ", "_"))
    return


def Tick():
    global activeRaffle, MySettings, raffle_start, warning_10, warning_30
    if activeRaffle:
        if MySettings.EnableEntryTimer and (time.time() - raffle_start) >= MySettings.EntryTimerDuration:
            CloseRaffle()
        elif MySettings.EnableWarningMessages and not warning_30 \
                and 30 <= (MySettings.EntryTimerDuration - (time.time() - raffle_start)) < 31:
            Parent.SendTwitchMessage(MySettings.Response_TimerWarning.format(30))
            warning_30 = True
        elif MySettings.EnableWarningMessages and not warning_10 \
                and 10 <= (MySettings.EntryTimerDuration - (time.time() - raffle_start)) < 11:
            Parent.SendTwitchMessage(MySettings.Response_TimerWarning.format(10))
            warning_10 = True
    return


def CloseRaffle():
    global activeRaffle, MySettings, raffleEntries
    if activeRaffle:
        activeRaffle = False
        Parent.SendTwitchMessage(MySettings.Response_RaffleClose)
    return


def ConfigureRaffle(
        cmd,
        ticket_cost,
        max_tickets,
        entry_duration,
        enable_warnings,
        allow_donations,
        allow_multiple_donations,
        currency_giveaway,
        currency_giveaway_amount,
        prize
    ):
    global MySettings, settingsFile
    MySettings.Command = cmd
    MySettings.TicketCost = int(ticket_cost)
    MySettings.MaxTickets = int(max_tickets)
    MySettings.Prize = prize
    MySettings.CurrencyGiveaway = True if int(currency_giveaway) == 1 else False
    MySettings.CurrencyGiveawayAmount = int(currency_giveaway_amount)
    MySettings.EnableDonations = True if int(allow_donations) == 1 else False
    MySettings.AllowMultipleDonations = True if int(allow_multiple_donations) == 1 else False
    MySettings.EnableEntryTimer = True if int(entry_duration) > 0 else False
    MySettings.EntryTimerDuration = int(entry_duration)
    MySettings.EnableWarningMessages = True if int(enable_warnings) == 1 else False
    MySettings.SaveSettings(settingsFile)
    return


def OpenRaffle():
    global activeRaffle, MySettings, raffle_start, warning_10, warning_30

    if not activeRaffle:
        activeRaffle = True
        raffle_start = time.time()
        warning_10 = False
        warning_30 = False
        donation_info = ""
        if MySettings.EnableDonations:
            donation_info = " " + MySettings.Response_DonateTickets.format(MySettings.Command)

        Parent.SendTwitchMessage("/me " + MySettings.Response_RaffleOpen.format(MySettings.Prize,
                                                                                MySettings.Permission))
        if MySettings.MaxTickets:
            full_message = "/me " + MySettings.Response_RaffleOpenWithMaxEntries.format(
                    MySettings.TicketCost, MySettings.MaxTickets, MySettings.Command)
        else:
            full_message = "/me " + MySettings.Response_RaffleOpenUnlimited.format(MySettings.TicketCost,
                                                                                    MySettings.Command)

        Parent.SendTwitchMessage(full_message + donation_info)
    return


def PickWinner(num):
    global raffleEntries, winnerList, activeRaffle
    if activeRaffle:
        Parent.SendTwitchMessage("/me You must close the raffle before choosing winners")
    else:
        if num == 0:
            num = 1

        if len(raffleEntries):
            for x in range(0, num):
                if len(raffleEntries):
                    # Randomize shuffling of entries
                    for i in range(1, randint(5, 25)):
                        shuffle(raffleEntries)

                    rand_index = randint(1, len(raffleEntries)) - 1
                    winner = raffleEntries[rand_index]
                    winnerList.append(winner)
                    RemoveEntrant(winner)
                    if MySettings.CurrencyGiveaway and MySettings.CurrencyGiveawayAmount > 0:
                        Parent.AddPoints(winner, int(MySettings.CurrencyGiveawayAmount))
                    Parent.SendTwitchMessage(MySettings.Response_Winner.format(Parent.GetDisplayName(winner)))
        else:
            Parent.SendTwitchMessage("/me There are no entrants to choose from")
    return


def PickSingleWinner():
    return PickWinner(1)

def PurchaseTickets(user, num):
    global MySettings, ticketPurchases
    if Parent.RemovePoints(user, MySettings.TicketCost * int(num)):
        if user not in ticketPurchases:
            ticketPurchases[user] = 0
        ticketPurchases[user] += num
        Parent.Log("Enhanced Raffle System", "Removed {0} {1} from {2} for {3} tickets".format(
              num * MySettings.TicketCost, Parent.GetCurrencyName(), user, num))
    else:
        Parent.SendTwitchMessage("@{0}: There was an error purchasing your tickets".format(Parent.GetDisplayName(user)))
        Parent.Log("Enhanced Raffle System", "Failed to remove {0} from {1}".format(num * MySettings.TicketCost, user))
    return


def RefundRaffleEntries():
    global activeRaffle, ticketPurchases, MySettings

    total_refunded = 0
    for user, num in ticketPurchases.items():
        total_purchased = MySettings.TicketCost * num
        # Parent.Log("Enhanced Raffle System", "Refunding {0} {1} {2}".format(user, total_purchased,
        #                                                                     Parent.GetCurrencyName()))
        if Parent.AddPoints(user, total_purchased):
            total_refunded += total_purchased
        else:
            Parent.Log("Enhanced Raffle System", "Failed to refund {0} {1} {2}".format(user, total_purchased,
                                                                                       Parent.GetCurrencyName()))

    Parent.SendTwitchMessage("/me " + MySettings.Response_RaffleRefund.format(Parent.GetCurrencyName()))

    activeRaffle = False
    return


def RemoveEntrant(username):
    global raffleEntries
    raffleEntries = filter(lambda x: x != username, raffleEntries)
    return


def ResetRaffle():
    global activeRaffle, donatedEntries, raffleEntries, MySettings, raffle_start, warning_10, warning_30
    global ticketPurchases
    activeRaffle = False
    raffle_start = 0
    warning_30 = False
    warning_10 = False
    raffleEntries = []
    ticketPurchases = {}
    donatedEntries = {}
    Parent.SendTwitchMessage("/me Raffle Reset")
    return


def SetPrize(prize):
    global MySettings
    MySettings.Prize = prize
    MySettings.SaveSettings(settingsFile)
    return


def ValidateTicketPurchase(user, num_tickets):
    global MySettings
    is_valid = True
    if MySettings.MaxTickets and num_tickets > MySettings.MaxTickets:
        is_valid = False
        Parent.SendTwitchMessage(MySettings.Response_ExceededMaxTickets.format(user, MySettings.MaxTickets))
    else:
        total_cost = MySettings.TicketCost * num_tickets
        if total_cost > Parent.GetPoints(user):
            is_valid = False
            Parent.SendTwitchMessage(MySettings.Response_NotEnoughCurrency.format(user, Parent.GetCurrencyName()))
    return is_valid

# AnkhBot Enhanced Raffle System

This raffle script mimics the GiveAway functionality of AnkhBot but
allows for chatters to enter the giveaway (raffle) on behalf of someone
else.

## Raffle Management

You can use the settings UI in AnkhBot to manage the raffle; or, you can
manage the raffle via commands through chat.

To manage the raffle through chat, use the `Raffle Management Command`
(default: `!manageraffle`) to configure different aspects of the raffle.
Available options to configure are:

  - `config` - Keyword use to configure the raffle in one command.  By
  default this is `config` but can be adjusted in the settings UI.
    - Parameters:
      - _Raffle Command_ - Command users use to enter raffle
      - _Ticket Cost_ - Cost per entry
      - _Maximum Tickets_ - Maximum number of tickets a user can
      purchase at once. Set to number of maximum tickets allowed or  `0`
      to have no maximum.
      - _Entry Duration_ - Sets a time limit to enter.  Automatically
      closes raffle after this many seconds. Set to number of seconds
      raffle should be open or `0` to disable.
      - _Enable Warnings_ - Enable 60, 30, and 10 second warnings before
      raffle closes.  Used with _Entry Duration_. Set to `1` to enable,
      `0` to disable.
      - _Allow Donations_ - Allow donating entries to other users. Set
      `1` to allow, `0` to disable.
      - _Allow Multiple Donations_ - Allow one person to donate to
      multiple people in the same raffle.
      - _Currency Giveaway_ - This raffle will automatically give an
      amount of currency to the chosen winner(s).
      - _Currency Giveaway Amount_ - The amount of currency to award
      each winner.
      - _Raffle Title / Prize_ - Set the title or prize of the raffle.
    - Examples:
      - `!manageraffle config !raffle 100 100 120 1 1 1 0 0 This is a test
      Raffle`
      - `!manageraffle config !raffle 100 0 120 0 1 0 1 1000 This is a test
      Raffle`
  - `snapshot` - Save a snapshot of current raffle settings.  Allows for
  future use of the same settings.
    - Parameters:
      - _Name_ - A name for the snapshot.
  - `load` - Load a saved snapshot of raffle settings.
    - Parameters:
      - _Name_ - Snapshot name.  Must be a name already used with the
      `snapshot` command to save a snapshot
  - `prize` - Update the name of the raffle
    - Parameters:
      - _Raffle Title / Prize_ - The name of the raffle or prize
  - `open` - Open a raffle to allow entries.
  - `close` - Close a raffle, no more entries allowed
  - `pick` - Choose a winner from the entries
  - `refund` - Refund all entrants their money, including any donated
  entries
  - `reset` - Clear all entries and winners and reset for another raffle
